package main

import (
	"flag"
	"fmt"
	"runtime"
	"time"
)

func cryer(done chan int) {
	for i := 0; i < runtime.NumCPU(); i++ {
		go func() {
			for {
				select {
				case <-done:
					return
				default:
				}
			}
		}()
	}
}

func underVoltageMode(seconds, peakSecond, bottomSecond int) {
	for start := time.Now(); time.Since(start) > time.Duration(seconds); {
		done := make(chan int)
		fmt.Println("Going to peek for ", peakSecond, " seconds")
		cryer(done)
		time.Sleep(time.Second * time.Duration(peakSecond))
		close(done)
		fmt.Println("Sleep for ", bottomSecond, " seconds")
		time.Sleep(time.Second * time.Duration(bottomSecond))
	}
}

func fullPeakMode(seconds int) {
	done := make(chan int)
	cryer(done)
	time.Sleep(time.Second * time.Duration(seconds))
	close(done)
}

func main() {
	var seconds int
	var underVoltageRegenerate bool
	var peakSecond int
	var bottomSecond int

	flag.IntVar(&seconds, "s", 10, "number of seconds to run the goroutines")
	flag.BoolVar(&underVoltageRegenerate, "uv", false, "activate under voltage generation mode")
	flag.IntVar(&peakSecond, "ps", 1, "peak duration(in seconds)-for undervoltage generation")
	flag.IntVar(&bottomSecond, "bs", 1, "bottom duration(in seconds)-for undervoltage generation")
	flag.Parse()

	fmt.Println("Parameters :")
	fmt.Println("--------------")

	fmt.Println("Running test for : ", seconds, " seconds")

	if underVoltageRegenerate {
		fmt.Println("Under voltage regeneration mode activated:", underVoltageRegenerate)
		fmt.Println("Peek seconds: ", peakSecond, " seconds")
		fmt.Println("Bottom seconds: ", bottomSecond, " seconds")
		fmt.Println("--------------")
		underVoltageMode(seconds, peakSecond, bottomSecond)
	} else {
		fmt.Println("--------------")
		fullPeakMode(seconds)
	}
}
